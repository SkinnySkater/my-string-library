/*
** my_strlen.c for  in /home/toruser/Piscine/C3/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Wed Oct 18 09:08:50 2017 LUGARD OVILA JULIEN
** Last update Wed Oct 18 09:24:18 2017 LUGARD OVILA JULIEN
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (*str)
    {
      str++;
      i++;
    }
  return (i);
}
