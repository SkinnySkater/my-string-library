/*
** my_swap.c for  in /home/toruser/Piscine/C3/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Wed Oct 18 09:28:07 2017 LUGARD OVILA JULIEN
** Last update Wed Oct 18 17:22:49 2017 LUGARD OVILA JULIEN
*/

void	my_swap(int *a, int *b)
{
  *a = *a ^ *b;
  *b = *a ^ *b;
  *a = *a ^ *b;
}
