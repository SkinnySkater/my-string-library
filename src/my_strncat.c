/*
** my_strncat.c for  in /home/toruser/Piscine/C4/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Thu Oct 19 13:04:52 2017 LUGARD OVILA JULIEN
** Last update Thu Oct 19 18:47:20 2017 LUGARD OVILA JULIEN
*/

int	mystrlen(char *str)
{
  int	i;

  i = 0;
  while (*str)
    {
      str++;
      i++;
    }
  return (i);
}

char	*my_strncat(char *dest, char *src, int n)
{
  int	len;
  int	i;

  len = mystrlen(dest);
  i = 0;
  while (src[i] && i < n)
    {
      dest[len + i] = src[i];
      i++;
    }
  dest[len + i] = '\0';
  return (dest);
}
