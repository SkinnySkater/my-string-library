/*
** get_next_alpha.c for  in /home/toruser/Piscine/C6/lugard_o/my_str_to_wordtab
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 27 03:15:33 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 03:16:11 2017 LUGARD OVILA JULIEN
*/

int	is_alpha(char s)
{
  return ((s > 64 && s < 91) || (s > 96 && s < 123));
}

int	is_num(char s)
{
  return (s > 47 && s < 58);
}

char	*get_next_alpha(char *s)
{
  while (*s && !(is_alpha(*s) || is_num(*s)))
    ++s;
  return (s);
}
