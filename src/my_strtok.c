#include "util.h"

char* my_strtok(char *str, const char* delim)
{
    static char* buf;
    if (str != NULL)
    	buf = str;
    if (buf[0] == '\0')
    	return NULL;
    char *ret = buf, *b;
    const char *d;
    for (b = buf; *b !='\0'; b++)
    {
        for (d = delim; *d != '\0'; d++)
        {
            if (*b == *d)
            {
                *b = '\0';
                buf = b + 1;
                if (b == ret)
                    ret++;
                return ret;
            }
        }
    }
    return ret;
}