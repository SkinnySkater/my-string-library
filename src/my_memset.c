#include "util.h"

void *my_memset(void *str, int c, size_t n)
{
    unsigned char *buf = (unsigned char *)str;
    while (n--)
    {
        *buf++ = (unsigned char)c;
    }
    return str;
}