/*
** my_string.c for  in /home/toruser/Piscine/FTL/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Mon Nov  6 09:37:29 2017 LUGARD OVILA JULIEN
** Last update Thu Nov  9 17:34:32 2017 LUGARD OVILA JULIEN
*/
#include		"util.h"

const char		*reset_color = "\033[0m";

int my_cstrlen(const char *str)
{
  int i;

  i = 0;
  while (*str)
    {
      str++;
      i++;
    }
  return (i);
}

void			my_cputstr(const char *str)
{
  write(1, str, my_cstrlen(str));
}

int			my_cstrcmp(const char *s1, const char *s2)
{
  int			i;

  if (s1 == NULL || s2 == NULL)
    return (-2);
  i = 0;
  while (s1[i] != '\0')
    {
      if (s1[i] > s2[i])
	return (1);
      else if (s1[i] < s2[i])
	return (-1);
      i++;
    }
  if (s2[i] != '\0')
    return (-1);
  return (0);
}

char			*my_strdup(const char *str)
{
  int			i;
  char			*copy;
  
  i = 0;
  copy = NULL;
  if ((copy = malloc((my_cstrlen(str) + 1) * sizeof(char))) == NULL)
    return (NULL);
  while (str[i] != '\0')
    {
      copy[i] = str[i];
      i++;
    }
  copy[i] = '\0';
  return (copy);
}

void			my_putstr_color(const char *color, const char *str)
{
  int			i;

  i = 0;
  while (g_color[i].color != NULL && (my_cstrcmp(g_color[i].color, color) != 0))
    i++;
  if (g_color[i].color == NULL)
    {
      my_cputstr(str);
      return ;
    }
  my_cputstr(g_color[i].unicode);
  my_cputstr(str);
  my_cputstr(reset_color);
}
