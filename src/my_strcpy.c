/*
** my_strcpy.c for  in /home/toruser/Piscine/C4/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Thu Oct 19 11:10:32 2017 LUGARD OVILA JULIEN
** Last update Thu Oct 19 18:08:39 2017 LUGARD OVILA JULIEN
*/

int	my_strlen2(char *str)
{
  int	i;

  i = 0;
  while (*str)
    {
      str++;
      i++;
    }
  return (i);
}

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (i < my_strlen2(src))
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}
