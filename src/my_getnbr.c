/*
** my_getnbr.c for  in /home/toruser/Piscine/C5/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 20 09:25:00 2017 LUGARD OVILA JULIEN
** Last update Mon Oct 23 12:25:56 2017 LUGARD OVILA JULIEN
*/

int	my_getnbr(char *str)
{
  int	i;
  int	res;
  int	s;

  s = 1;
  res = 0;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    {
      if (str[i] == '-')
	s = -s;
      i++;
    }
  while (str[i] != '\0' && str[i] >= '0' && str[i] <= '9')
    {
      res = (res * 10) + (str[i] - 48);
      i++;
    }
  return (s * res);
}
