/*
** my_strcat.c for  in /home/toruser/Piscine/C4/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Thu Oct 19 12:39:05 2017 LUGARD OVILA JULIEN
** Last update Thu Oct 19 21:29:03 2017 LUGARD OVILA JULIEN
*/

int	my_strlen3(char *str)
{
  int	i;

  i = 0;
  while (*str)
    {
      str++;
      i++;
    }
  return (i);
}

char	*my_strcat(char *dest, char *src)
{
  int	len;
  int	i;

  len = my_strlen3(dest);
  i = 0;
  while (src[i])
    {
      dest[len + i] = src[i];
      i++;
    }
  dest[len + i] = '\0';
  return (dest);
}
