/*
** my_str_to_wordtab.c for  in /home/toruser/Piscine/C6/lugard_o/my_str_to_wordtab
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Tue Oct 24 03:17:29 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 03:18:39 2017 LUGARD OVILA JULIEN
*/

#include <stdlib.h>

int     my_strlen(char *str);

void	my_put_nbr(int nb);

char    *my_strncpy(char *dest, char *src, int n);

char	*get_next_alpha(char *s);

char	*get_next_no_alpha(char *s);

int	count_word(char *str)
{
  char	*cur;
  int	n_word;

  cur = get_next_alpha(str);
  n_word = 1;
  while (cur < str + my_strlen(str))
    {
      ++n_word;
      cur = get_next_alpha(cur);
      cur = get_next_no_alpha(cur + 1);
    }
  return (n_word);
}

char**	check_end(char **res)
{
  int i;

  i = 0;
  while (res[i])
  {
    if (res[i][0] == '\0')
      res[i] = NULL;
    i++;
  }
  return (res);
}

char    **my_str_to_wordtab(char *str)
{
  char	**res;
  char	*cur;
  int	n_word;
  int	i;
  
  if (!str)
    return (NULL);
  n_word = count_word(str);
  res = malloc(sizeof(*res) * n_word);
  res[n_word - 1] = 0;
  cur = get_next_alpha(str);
  i = 0;
  while (++i < n_word)
    {
      str = get_next_alpha(str);
      cur = get_next_no_alpha(str + 1);
      res[i - 1] = malloc(sizeof(*(res[i - 1])) * (cur - str + 1));
      res[i - 1][cur - str] = '\0';
      my_strncpy(res[i - 1], str, (cur - str));
      str = cur + 1;
    }
  return (check_end(res));
}
