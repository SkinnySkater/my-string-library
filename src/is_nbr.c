/*
** is_nbr.c for  in /home/toruser/Piscine/SCHIFUMI/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 27 09:53:33 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 09:57:53 2017 LUGARD OVILA JULIEN
*/
#include "util.h"

int	char_isnum(char s)
{
  return (s > 47 && s < 58);
}

int	is_nbr(char *str)
{
  int	i;
  int	len;

  len = my_strlen(str);
  i = 0;
  if (str[i] != '-' && !char_isnum(str[i]))
    return (i == len);
  while (i < len && char_isnum(str[i]))
    i++;
  return (i == len);
}
