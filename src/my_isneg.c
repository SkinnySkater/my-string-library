/*
** my_isneg.c for  in /home/toruser/Piscine/C1/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Mon Oct 16 15:13:51 2017 LUGARD OVILA JULIEN
** Last update Mon Oct 16 23:07:46 2017 LUGARD OVILA JULIEN
*/

int	my_isneg(int x)
{
  if (x < 0)
    {
      return (1);
    }
  else
    {
      return (0);
    }
}
