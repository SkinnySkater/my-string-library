/*
** get_next_no_alpha.c for  in /home/toruser/Piscine/C6/lugard_o/my_str_to_wordtab
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 27 03:16:50 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 03:20:25 2017 LUGARD OVILA JULIEN
*/

int	is_alpha1(char s)
{
  return ((s > 64 && s < 91) || (s > 96 && s < 123));
}

int	is_num1(char s)
{
  return (s > 47 && s < 58);
}

char	*get_next_no_alpha(char *s)
{
  while (*s && (is_alpha1(*s) || is_num1(*s)))
    ++s;
  return (s);
}
