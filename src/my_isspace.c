#include "util.h"

int	my_isspace(char *s)
{
  while (*s)
	{
		if (*s != ' ' && *s != '\n' && *s != '\t'
			&& *s != '\v' && *s != '\f' && '\r')
			return (0);
	  	s++;
	}
  return (1);
}
