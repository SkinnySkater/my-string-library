CC=gcc

CFLAGS=-Wall -Wextra -Werror  -W

DIRLIB=libmy/

SRC=src/my_strcmp.c src/my_putstr.c src/my_putchar.c\
			src/my_strlen.c src/my_getnbr.c src/is_nbr.c\
			src/my_put_nbr.c src/readline.c src/my_string.c\
			src/my_cls.c src/my_strcat.c src/my_memset.c\
			src/my_perror.c src/my_exit.c src/my_strtok.c\
			src/get_next_alpha.c src/get_next_no_alpha.c\
			src/my_isneg.c src/my_isspace.c src/my_strcpy.c\
			src/my_str_to_wordtab.c src/my_strdup.c\
			src/my_strncat.c src/my_strncpy.c src/my_strstr.c\
			src/my_strtok.c src/my_swap.c

OBJ=$(SRC:.c=.o)

AR=ar

CSR=csr

LIB=libmy.a

.PHONY	:	fclean clean all re

all	:	$(LIB)

$(LIB)	:	$(OBJ)
	$(AR) $(CSR) $@  $(OBJ)
		  ranlib $(LIB)

clean	:
	$(RM) $(OBJ) *.swp

fclean	:	clean
	$(RM) $(LIB)

re	:	fclean all
